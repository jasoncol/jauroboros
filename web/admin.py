from django.contrib import admin
from web.models import News, Album, Track, Video, RelatedImage, Integrante, FotoIntegrante, NewsTags, VideoTags

# Register your models here.

class TrackInline(admin.TabularInline):
    model = Track

class AlbumCoverInline(admin.TabularInline):
    model = RelatedImage

class AlbumAdmin(admin.ModelAdmin):
    model = Album
    inlines = [AlbumCoverInline, TrackInline]

class ImageInline(admin.TabularInline):
    model = RelatedImage

class NewsTagsInline(admin.TabularInline):
    model = NewsTags

class VideoTagsInline(admin.TabularInline):
    model = VideoTags


class NewsAdmin(admin.ModelAdmin):
    model = News
    inlines = [ImageInline, NewsTagsInline]


class FotoIntegranteInline(admin.TabularInline):
    model = FotoIntegrante

class IntengranteAdmin(admin.ModelAdmin):
    model = Integrante
    inlines = [FotoIntegranteInline]

class VideoAdmin(admin.ModelAdmin):
    model = Video
    inlines = [VideoTagsInline]

admin.site.register(News, NewsAdmin)
admin.site.register(Album, AlbumAdmin)
admin.site.register(Integrante, IntengranteAdmin)
admin.site.register(Track)
admin.site.register(Video, VideoAdmin)
