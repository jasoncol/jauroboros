from django.db import models
from django_images import models as di_models
from django.utils.html import mark_safe
from django.utils.text import slugify

# Create your models here.
class BaseModel(models.Model):
    title = models.CharField("Title", max_length=150)
    content = models.TextField("Content", default='')
    date = models.DateField("Date")
    visible = models.BooleanField(default=False)

    class Meta:
        ordering = ['-date']
    
    def slug(self):
        return slugify(self.title)

    def __unicode__(self):
        return self.title

class RelatedImage(di_models.Image):
    entry = models.ForeignKey(BaseModel, related_name='images')

    '''
    def get_by_size(self, size):
        t = [x for x in self.thumbnail_set.all() if x.size == size]
        if len(t):
            return t[0]
        else: 
            return self.thumbnail_set.get(size=size)

    def __unicode__(self):
        html = '<img src="%s" alt=""/>' % (
            self.get_absolute_url('admin'),)
        return mark_safe(html)
    '''

class News(BaseModel):
    pass


class Album(BaseModel):
    play_link = models.URLField()
    appstore_link = models.URLField()
    

class Track(models.Model):
    album = models.ForeignKey(Album, related_name='tracks')
    name = models.CharField("Name", max_length=150)

class Video(BaseModel):
    link = models.URLField()
    
    def get_thumbnail(self):
        l = self.link

        return "//i.ytimg.com/vi/%s/mqdefault.jpg"%(l.split('/')[-1])
    
    def get_code(self):
        return "%s"%(self.link.split('/')[-1])

    def slug(self):
        return slugify(self.title)

class Integrante(models.Model):
    name = models.CharField("Name", max_length=150)
    content = models.TextField("Content", default='')

    def __unicode__(self):
        return self.name

class FotoIntegrante(di_models.Image):
    integrante = models.ForeignKey(Integrante, related_name='images')



class VideoTags(models.Model):
    name = models.CharField("Valor", max_length=50)
    video = models.ForeignKey(Video)


class NewsTags(models.Model):
    name = models.CharField("Valor", max_length=50)
    news = models.ForeignKey(News)
