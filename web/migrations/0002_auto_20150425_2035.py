# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('django_images', '__first__'),
        ('web', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='FotoIntegrante',
            fields=[
                ('image_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='django_images.Image')),
            ],
            options={
            },
            bases=('django_images.image',),
        ),
        migrations.CreateModel(
            name='Integrante',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=150, verbose_name=b'Name')),
                ('content', models.TextField(default=b'', verbose_name=b'Content')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='fotointegrante',
            name='integrante',
            field=models.ForeignKey(to='web.Integrante'),
            preserve_default=True,
        ),
    ]
