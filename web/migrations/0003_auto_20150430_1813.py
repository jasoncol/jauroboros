# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('web', '0002_auto_20150425_2035'),
    ]

    operations = [
        migrations.CreateModel(
            name='NewsTags',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=50, verbose_name=b'Valor')),
                ('news', models.ForeignKey(to='web.News')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='VideoTags',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=50, verbose_name=b'Valor')),
                ('video', models.ForeignKey(to='web.Video')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.AlterModelOptions(
            name='basemodel',
            options={'ordering': ['-date']},
        ),
        migrations.AlterField(
            model_name='fotointegrante',
            name='integrante',
            field=models.ForeignKey(related_name='images', to='web.Integrante'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='relatedimage',
            name='entry',
            field=models.ForeignKey(related_name='images', to='web.BaseModel'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='track',
            name='album',
            field=models.ForeignKey(related_name='tracks', to='web.Album'),
            preserve_default=True,
        ),
    ]
