# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('django_images', '__first__'),
    ]

    operations = [
        migrations.CreateModel(
            name='BaseModel',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=150, verbose_name=b'Title')),
                ('content', models.TextField(default=b'', verbose_name=b'Content')),
                ('date', models.DateField(verbose_name=b'Date')),
                ('visible', models.BooleanField(default=False)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Album',
            fields=[
                ('basemodel_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='web.BaseModel')),
                ('play_link', models.URLField()),
                ('appstore_link', models.URLField()),
            ],
            options={
            },
            bases=('web.basemodel',),
        ),
        migrations.CreateModel(
            name='News',
            fields=[
                ('basemodel_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='web.BaseModel')),
            ],
            options={
            },
            bases=('web.basemodel',),
        ),
        migrations.CreateModel(
            name='RelatedImage',
            fields=[
                ('image_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='django_images.Image')),
            ],
            options={
            },
            bases=('django_images.image',),
        ),
        migrations.CreateModel(
            name='Track',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=150, verbose_name=b'Name')),
                ('album', models.ForeignKey(to='web.Album')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Video',
            fields=[
                ('basemodel_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='web.BaseModel')),
                ('link', models.URLField()),
            ],
            options={
            },
            bases=('web.basemodel',),
        ),
        migrations.AddField(
            model_name='relatedimage',
            name='entry',
            field=models.ForeignKey(to='web.BaseModel'),
            preserve_default=True,
        ),
    ]
