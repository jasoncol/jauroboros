from django.shortcuts import render
from django.views.generic import TemplateView, DetailView, ListView
from models import Album, News, Video

# Create your views here.

class AlbumListMixin(object):
    def get_context_data(self, **kwargs):
        context = super(AlbumListMixin, self).get_context_data(**kwargs)
        context['album_list'] = Album.objects.filter(visible=True)
        return context

class HomeView(AlbumListMixin, TemplateView):
    template_name = 'web/index.html'

    def get_context_data(self, **kwargs):
        context = super(HomeView, self).get_context_data(**kwargs)
        news = News.objects.filter(visible=True)[:8]
        context['news_list'] = news[1:]
        context['video_list'] = Video.objects.filter(visible=True)[:8]
        context['first_news'] =  news[0]
        return context

class AlbumView(AlbumListMixin, DetailView):
    template_name='web/discos.html'
    model = Album

    def get_object(self, queryset=None):
        if 'pk' not in self.kwargs:
            obj = Album.objects.filter(visible=True)[0]
            return obj
        return super(AlbumView, self).get_object(queryset)

    def get_context_data(self, **kwargs):
        context = super(AlbumView, self).get_context_data(**kwargs)
        related_videos = Video.objects.filter(videotags__name=self.get_object().title.lower())
        context['video_list'] = related_videos
        return context


class VideoView(DetailView):
    template_name='web/videos.html'
    model=Video

    def get_object(self, queryset=None):
        if 'pk' not in self.kwargs:
            obj = Video.objects.filter(visible=True)[0]
            return obj
        return super(VideoView, self).get_object(queryset)

    def get_context_data(self, **kwargs):
        context = super(VideoView, self).get_context_data(**kwargs)
        context['video_list'] = Video.objects.filter(visible=True)
        return context
