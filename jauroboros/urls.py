from django.conf.urls import patterns, include, url
from django.contrib import admin
from django.views.generic import TemplateView, DetailView, ListView
from django.conf import settings
from django.conf.urls.static import static
from web import views
from web.models import News, Integrante, Video

urlpatterns = patterns('',
    # Examples:
     url(r'^$', views.HomeView.as_view(), name='home'),
     url(r'^biografia/', ListView.as_view(template_name='web/about.html', model=Integrante), name='about'),
     url(r'^discografia/$', views.AlbumView.as_view(), name='discos'),
     url(r'^discografia/(?P<slug>[-_\w]+)-(?P<pk>[0-9]{1,5})', views.AlbumView.as_view(), name='disco_detail'),
     url(r'^videos/$', views.VideoView.as_view(), name='videos'),
     url(r'^videos/(?P<slug>[-_\w]+)-(?P<pk>[0-9]{1,5})', views.VideoView.as_view(), name='video_detail'),
     url(r'^noticias/$', ListView.as_view(template_name='web/news_list.html', model=News), name='noticias'),
     url(r'^noticias/(?P<slug>[-_\w]+)-(?P<pk>[0-9]{1,5})', DetailView.as_view(template_name='web/news_detail.html', model=News), name='noticia'),
     url(r'^admin/', include(admin.site.urls)),
     url(r'^images/', include('django_images.urls')),
) + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT) + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
